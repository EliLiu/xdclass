package com.nb.xdclass.utils;


/**
 * 响应给前端的数据格式
 */
public class JsonData {
    /**
     * 状态码:
     * 0:成功
     * 1:处理中
     * -1:失败
     */
    private Integer code;

    /**
     * 业务数据
     */
    private Object data;

    /**
     * 信息表示
     */
    private String message;

    /**
     * 成功, 不响应数据
     * @return
     */
    public static JsonData buildSuccess(){
        return new JsonData(0,null,null);
    }

    /**
     * 成功. 响应数据
     * @param data
     * @return
     */
    public static JsonData buildSuccess(Object data){
        return new JsonData(0,data,null);
    }

    /**
     * 失败, 固定状态码
     * @param message
     * @return
     */
    public static JsonData buildError(String message){
        return new JsonData(-1,null,message);
    }

    /**
     * 失败,自定义状态码
     * @param code
     * @param message
     * @return
     */
    public static JsonData buildError(Integer code,String message){
        return new JsonData(code,null,message);
    }



    public JsonData(Integer code, Object data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public JsonData() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
