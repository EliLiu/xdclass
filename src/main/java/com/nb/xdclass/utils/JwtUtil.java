package com.nb.xdclass.utils;

import com.nb.xdclass.entity.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * JWT工具类
 */
public class JwtUtil {

    /**
     * 过期时间
     */
    private static final long EXPIRATION = 60000 * 60 * 24 * 7;

    /**
     * 加密秘钥
     */
    private static final String SECRET = "nb.com";

    /**
     * 令牌前缀
     */
    private static final String TOKEN_PREFIX = "nb";

    /**
     * subject
     */
    private static final String SUBJECT = "nb";


    /**
     * 生成token
     *
     * @param user
     * @return
     */
    public static String generateJsonWebToken(User user) {
        /**
         * setSubject() 设置主题
         * claim() 要加密的元素
         * setIssuedAt() 颁发时间
         * setIssuedAt() 设置到期时间
         * signWith() 设置加密算法, 并加上秘钥
         */
        String token = Jwts.builder().setSubject(SUBJECT)
                .claim("head_img", user.getHeadImg())
                .claim("id", user.getId())
                .claim("name", user.getName())
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() +
                        EXPIRATION))
                .signWith(SignatureAlgorithm.HS256, SECRET).compact();
        token = TOKEN_PREFIX + token;

        return token;
    }

    /**
     * 校验token,
     * 校验失败返回null
     *
     * @param token
     * @return
     */
    public static Claims checkJWT(String token) {
        /**
         * setSigningKey() 设置解析秘钥
         * replace() 替换前缀
         */
        try {

            final Claims claims = Jwts.parser().setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, "")).getBody();

            return claims;

        } catch (Exception e) {
            return null;
        }

    }


}
