package com.nb.xdclass.mapper;

import com.nb.xdclass.entity.model.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    /**
     * 根据phone查询
     *
     * @param phone
     * @return
     */
    User findByPhone(@Param("phone") String phone);

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    int save(User user);

    /**
     * 根据phone和pwd查询
     *
     * @param phone
     * @param pwd
     * @return
     */
    User findByPhoneAndPwd(@Param("phone") String phone, @Param("pwd") String pwd);

    /**
     *根据id查询user
     *
     * @param id
     * @return
     */
    User findById(@Param("id") Integer id);
}
