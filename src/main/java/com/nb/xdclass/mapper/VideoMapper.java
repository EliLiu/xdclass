package com.nb.xdclass.mapper;

import com.nb.xdclass.entity.model.Episode;
import com.nb.xdclass.entity.model.Video;
import com.nb.xdclass.entity.model.VideoBanner;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VideoMapper {

    /**
     * 获取全部视频
     *
     * @return
     */
    List<Video> videoList();

    /**
     * 获取全部轮播
     *
     * @return
     */
    List<VideoBanner> bannerList();

    /**
     * 获取视频详情
     *
     * @param videoId
     * @return
     */
    Video findDetailsByid(@Param("video_id") Integer videoId);

    /**
     * 获取视频简单信息
     *
     * @param videoId
     * @return
     */
    Video findSimpleById(@Param("video_id") Integer videoId);

    /**
     * 获取视频第一集
     * @param videoId
     * @return
     */
    Episode findEpisodeFirstByVideoId(@Param("video_id") Integer videoId);
}
