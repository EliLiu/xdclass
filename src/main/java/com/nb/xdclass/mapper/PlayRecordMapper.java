package com.nb.xdclass.mapper;

import com.nb.xdclass.entity.model.PlayRecord;

public interface PlayRecordMapper {

    /**
     * 插入一条播放记录
     * @param playRecord
     */
    void save(PlayRecord playRecord);
}
