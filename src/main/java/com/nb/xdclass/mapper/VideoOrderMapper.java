package com.nb.xdclass.mapper;

import com.nb.xdclass.entity.model.VideoOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VideoOrderMapper {

    /**
     * 根据userId  videoId status查找 videoOrder
     *
     * @param userId
     * @param videoId
     * @param status
     * @return
     */
    VideoOrder findByUserIdAndVideoIdAndStatus(@Param("user_id") Integer userId, @Param("video_id") Integer videoId,
                                               @Param("state") Integer state);

    /**
     * 添加VideoOrder
     *
     * @return
     */
    int save(VideoOrder videoOrder);

    /**
     * 获取订单列表
     * @param userId
     * @return
     */
    List<VideoOrder> listOrderByUserId(@Param("user_id") Integer userId);
}
