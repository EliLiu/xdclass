package com.nb.xdclass.controller;

import com.nb.xdclass.entity.model.Video;
import com.nb.xdclass.entity.model.VideoBanner;
import com.nb.xdclass.service.VideoService;
import com.nb.xdclass.utils.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/pub/video")
public class VideoController {

    @Autowired
    VideoService videoService;

    /**
     * 轮播图列表接口
     * @return
     */
    @GetMapping("list_banner")
    public JsonData bannerList(){
        List<VideoBanner> bannerList = videoService.bannerList();
        return JsonData.buildSuccess(bannerList);
    }

    /**
     * 视频列表接口
     * @return
     */
    @RequestMapping("list")
    public JsonData videoList() {

        List<Video> videos = videoService.videoList();
        return JsonData.buildSuccess(videos);
    }

    /**
     * 视频详情接口
     * @param videoId
     * @return
     */
    @GetMapping("details_by_id")
    public JsonData videoDetails(@RequestParam(value = "video_id",required = true) int videoId){
        Video video = videoService.findDetailsByid(videoId);

        return JsonData.buildSuccess(video);
    }

}
