package com.nb.xdclass.controller;

import com.nb.xdclass.entity.model.VideoOrder;
import com.nb.xdclass.entity.request.VideoOrderRequest;
import com.nb.xdclass.service.VideoOrderService;
import com.nb.xdclass.utils.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("api/v1/pri/order")
public class VideoOrderController {

    @Autowired
    VideoOrderService videoOrderService;

    /**
     * 下单接口
     *
     * @param videoOrderRequest
     * @param request
     * @return
     */
    @PostMapping("save")
    public JsonData saveOrder(@RequestBody VideoOrderRequest videoOrderRequest, HttpServletRequest request) {

        Integer videoId = videoOrderRequest.getVideoId();
        Integer userId = (Integer) request.getAttribute("user_id");
        return videoOrderService.save(videoId, userId) == 1 ? JsonData.buildSuccess() : JsonData.buildError("下单失败");
    }

    /**
     * 订单列表接口
     *
     * @return
     */
    @GetMapping("list")
    public JsonData list(HttpServletRequest request) {
        Integer userId = (Integer) request.getAttribute("user_id");
        List<VideoOrder> videoOrderList = videoOrderService.listOrderByUserId(userId);
        return JsonData.buildSuccess(videoOrderList);
    }
}
