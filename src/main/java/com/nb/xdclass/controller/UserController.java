package com.nb.xdclass.controller;

import com.nb.xdclass.entity.model.User;
import com.nb.xdclass.entity.request.LoginRequest;
import com.nb.xdclass.service.UserService;
import com.nb.xdclass.utils.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("api/v1/pri/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 注册接口
     *
     * @param userInfo
     * @return
     */
    @PostMapping("register")
    public JsonData save(@RequestBody Map<String, String> userInfo) {

        int rows = userService.save(userInfo);
        return rows == 1 ? JsonData.buildSuccess() : JsonData.buildError("注册失败");
    }

    /**
     * 登录接口
     * LoginRequest 登录请求参数的实体类, 也可直接使用map
     *
     * @param loginRequest
     * @return
     */
    @PostMapping("login")
    public JsonData login(@RequestBody LoginRequest loginRequest) {

        String token = userService.findByPhoneAndPwd(loginRequest.getPhone(), loginRequest.getPwd());
        return token != null ? JsonData.buildSuccess(token) : JsonData.buildError("登录失败");
    }

    /**
     * 查询用户信息接口
     *
     * @param request
     * @return
     */
    @GetMapping("find_by_token")
    public JsonData findByToken(HttpServletRequest request) {
        Integer id = (Integer) request.getAttribute("user_id");
        if (id == null) {
            return JsonData.buildError("查询失败");
        }
        User user = userService.findById(id);
        return JsonData.buildSuccess(user);
    }
}
