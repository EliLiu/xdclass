package com.nb.xdclass.entity.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 前端登录请求参数实体类
 */
public class LoginRequest {

    private String phone;

    private String pwd;
}
