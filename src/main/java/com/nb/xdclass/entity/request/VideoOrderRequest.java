package com.nb.xdclass.entity.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 下单请求参数实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VideoOrderRequest {

    /**
     *  @JsonProperty("video_id") 将json参数中的video_id映射为该属性 类似 @RequestParam(value="")
     */
    @JsonProperty("video_id")
    private Integer videoId;
}
