package com.nb.xdclass.exception;

import com.nb.xdclass.utils.JsonData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(Exception.class)
    public JsonData handler(Exception e) {
        if (e instanceof BaseException) {
            logger.error("业务异常",e);
            BaseException baseException = (BaseException) e;
            return JsonData.buildError(baseException.getCode(), baseException.getMsg());
        } else {
            return JsonData.buildError("未知异常");
        }
    }
}
