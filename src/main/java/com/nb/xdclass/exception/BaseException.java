package com.nb.xdclass.exception;

/**
 * 自定义异常
 * TODO 待改进
 */
public class BaseException extends RuntimeException{
    private Integer code;

    private String msg;

    public BaseException(){}

    public BaseException(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
