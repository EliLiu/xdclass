package com.nb.xdclass.service;

import com.nb.xdclass.entity.model.Video;
import com.nb.xdclass.entity.model.VideoBanner;

import java.util.List;

public interface VideoService {


    List<Video> videoList();

    List<VideoBanner> bannerList();

    Video findDetailsByid(int videoId);
}
