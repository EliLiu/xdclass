package com.nb.xdclass.service;

import com.nb.xdclass.entity.model.User;

import java.util.Map;

public interface UserService {
    /**
     * 根据phone查找User
     *
     * @param phone
     * @return
     */
    User findByPhone(String phone);


    /**
     * 注册保存User
     *
     * @param userInfo
     * @return
     */
    int save(Map<String, String> userInfo);

    /**
     * 登录查询user
     *
     * @param phone
     * @param pwd
     * @return 返回token
     */
    String findByPhoneAndPwd(String phone, String pwd);

    /**
     * 根据id查询user
     *
     * @param id
     * @return
     */
    User findById(Integer id);
}
