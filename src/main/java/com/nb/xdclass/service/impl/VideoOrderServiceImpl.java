package com.nb.xdclass.service.impl;

import com.nb.xdclass.entity.model.Episode;
import com.nb.xdclass.entity.model.PlayRecord;
import com.nb.xdclass.entity.model.Video;
import com.nb.xdclass.entity.model.VideoOrder;
import com.nb.xdclass.exception.BaseException;
import com.nb.xdclass.mapper.PlayRecordMapper;
import com.nb.xdclass.mapper.VideoMapper;
import com.nb.xdclass.mapper.VideoOrderMapper;
import com.nb.xdclass.service.VideoOrderService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@Transactional(rollbackFor = Exception.class)
public class VideoOrderServiceImpl implements VideoOrderService {

    @Resource
    VideoOrderMapper videoOrderMapper;

    @Resource
    VideoMapper videoMapper;

    @Resource
    PlayRecordMapper playRecordMapper;


    /**
     * 下单操作
     * TODO 未来版本: 优惠券抵扣, 风控用户检测, 生成订单基础信息, 生成支付信息
     *
     * @param videoId
     * @param userId
     * @return
     */
    @Override
    @Transactional(rollbackFor = BaseException.class)
    public int save(Integer videoId, Integer userId) {

        //根据videoIds, userId, status查找VideoOrder, 判断是否已购买
        VideoOrder videoOrder = videoOrderMapper.findByUserIdAndVideoIdAndStatus(userId, videoId, 1);
        if (videoOrder != null) {
            return 0;
        }
        //根据videoId 获取视频简单信息
        //构造订单对象
        Video video = videoMapper.findSimpleById(videoId);
        VideoOrder newVideoOrder = new VideoOrder();
        newVideoOrder.setCreateTime(new Date());
        newVideoOrder.setOutTradeNo(UUID.randomUUID().toString());
        newVideoOrder.setState(1);
        newVideoOrder.setTotalFee(video.getPrice());
        newVideoOrder.setUserId(userId);
        newVideoOrder.setVideoId(videoId);
        newVideoOrder.setVideoImg(video.getCoverImg());
        newVideoOrder.setVideoTitle(video.getTitle());
        int rows = videoOrderMapper.save(newVideoOrder);
        //下单成功,插入视频播放记录
        if (rows == 1) {
            asyncSavePlayRecord(videoId, userId);
        }
        return rows;
    }

    @Override
    public List<VideoOrder> listOrderByUserId(Integer userId) {
        return videoOrderMapper.listOrderByUserId(userId);
    }

    /**
     * 异步添加播放记录
     * @param videoId
     * @param userId
     */
    @Async
    public void asyncSavePlayRecord(Integer videoId, Integer userId){
        //获取当前视频的第一集
        Episode episode = videoMapper.findEpisodeFirstByVideoId(videoId);
        if (null == episode) {
            throw new BaseException(-1, "数据异常,请联系运营");
        }
        //构造PlayRecord
        PlayRecord playRecord = new PlayRecord();
        playRecord.setCurrentNum(episode.getNum());
        playRecord.setCreateTime(new Date());
        playRecord.setEpisodeId(episode.getId());
        playRecord.setVideoId(videoId);
        playRecord.setUserId(userId);
        playRecordMapper.save(playRecord);
    }
}
