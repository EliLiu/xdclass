package com.nb.xdclass.service.impl;

import com.nb.xdclass.entity.model.User;
import com.nb.xdclass.mapper.UserMapper;
import com.nb.xdclass.service.UserService;
import com.nb.xdclass.utils.JwtUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
import java.util.Random;

@Service("userService")
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User findByPhone(String phone) {
        return userMapper.findByPhone(phone);
    }

    @Override
    public int save(Map<String, String> userInfo) {
        User user = parse2User(userInfo);
        if (null != user) {
            return userMapper.save(user);
        }
        return -1;
    }

    @Override
    public String findByPhoneAndPwd(String phone, String pwd) {

        pwd = DigestUtils.md5DigestAsHex(pwd.getBytes());
        User user = userMapper.findByPhoneAndPwd(phone, pwd);
        if (null != user) {
            String token = JwtUtil.generateJsonWebToken(user);
            return token;
        }
        return null;
    }

    @Override
    public User findById(Integer id) {
        User user = userMapper.findById(id);
        return user;
    }


    private User parse2User(Map<String, String> userInfo) {
        if (!(userInfo.containsKey("name") && userInfo.containsKey("pwd") && userInfo.containsKey("phone"))) {
            return null;
        }
        String name = userInfo.get("name");
        String phone = userInfo.get("phone");
        String pwd = userInfo.get("pwd");
        //MD5
        pwd = DigestUtils.md5DigestAsHex(pwd.getBytes());
        User user = new User();
        user.setName(name);
        user.setPhone(phone);
        user.setPwd(pwd);
        user.setHeadImg(randImg());
        user.setCreateTime(new Date());
        return user;
    }

    private static final String[] headImg = {
            "https://xd-video-pc-img.oss-cn-beijing.aliyuncs.com/xdclass_pro/default/head_img/12.jpeg",
            "https://xd-video-pc-img.oss-cn-beijing.aliyuncs.com/xdclass_pro/default/head_img/11.jpeg",
            "https://xd-video-pc-img.oss-cn-beijing.aliyuncs.com/xdclass_pro/default/head_img/13.jpeg",
            "https://xd-video-pc-img.oss-cn-beijing.aliyuncs.com/xdclass_pro/default/head_img/14.jpeg",
            "https://xd-video-pc-img.oss-cn-beijing.aliyuncs.com/xdclass_pro/default/head_img/15.jpeg",
    };

    /**
     * 随机用户头像
     *
     * @return
     */
    private String randImg() {
        int size = headImg.length;
        Random random = new Random();
        int index = random.nextInt(size);
        return headImg[index];
    }


}
