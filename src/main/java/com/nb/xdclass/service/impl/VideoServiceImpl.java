package com.nb.xdclass.service.impl;

import com.nb.xdclass.config.CacheKeyManager;
import com.nb.xdclass.entity.model.Video;
import com.nb.xdclass.entity.model.VideoBanner;
import com.nb.xdclass.mapper.VideoMapper;
import com.nb.xdclass.service.VideoService;
import com.nb.xdclass.utils.BaseCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
@Transactional(rollbackFor = Exception.class)
public class VideoServiceImpl implements VideoService {

    @Resource
    private VideoMapper videoMapper;

    @Autowired
    private BaseCache baseCache;

    /**
     * 获取全部视频
     *
     * @return
     */
    @Override
    public List<Video> videoList() {
        try {

            Object cacheObj = baseCache.getTenMinuteCache().get(CacheKeyManager.INDEX_VIDEO_LIST, () -> {
                return videoMapper.videoList();
            });

            if (cacheObj instanceof List) {
                return (List<Video>) cacheObj;
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        //TODO 可以返回兜底数据 本地模拟数据
        return null;
    }

    /**
     * 获取全部轮播
     *
     * @return
     */
    @Override
    public List<VideoBanner> bannerList() {
        try {
            /**
             * 判断缓存中是否有 CacheKeyManager.INDEX_BANNER_KEY对应的值,
             * 有则返回值
             * 没有则执行回调函数中的语句, 并将回调函数中的结果返回
             */
            Object cacheObj = baseCache.getTenMinuteCache().get(CacheKeyManager.INDEX_BANNER_KEY, () -> {
                System.out.println("查询数据路中的数据");
                return videoMapper.bannerList();
            });

            if (cacheObj instanceof List) {
                return (List<VideoBanner>) cacheObj;
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取视频详情
     *
     * @param videoId
     * @return
     */
    @Override
    public Video findDetailsByid(int videoId) {
        String videoDetailKey = String.format(CacheKeyManager.VIDEO_DETAIL, videoId);

        try {

            Object cacheObj = baseCache.getOneHourCache().get(videoDetailKey, () -> {
                return videoMapper.findDetailsByid(videoId);
            });

            if (cacheObj instanceof Video) {
                return (Video) cacheObj;
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}
