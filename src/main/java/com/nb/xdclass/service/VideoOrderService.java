package com.nb.xdclass.service;

import com.nb.xdclass.entity.model.VideoOrder;

import java.util.List;

public interface VideoOrderService {

    /**
     * 保存videoOrder
     * @param
     * @return
     */
    int save(Integer videoId, Integer userId);

    /**
     * 获取订单列表
     * @param userId
     * @return
     */
    List<VideoOrder> listOrderByUserId(Integer userId);
}
