package com.nb.xdclass.config;

/**
 * 缓存key管理类
 * 模块之间使用 ":" 分隔
 *
 * @author eliliu
 */
public class CacheKeyManager {

    /**
     * 首页轮播列表key
     */
    public static final String INDEX_BANNER_KEY = "index:banner:list";

    /**
     * 首页视频列表key
     */
    public static final String INDEX_VIDEO_LIST = "index:video:list";

    /**
     * 视频详情key
     * %s 视频id
     */
    public static final String VIDEO_DETAIL = "video:detail:%s";
}
