package com.nb.xdclass.config;

import com.nb.xdclass.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class LoginInterceptorConfigurer implements WebMvcConfigurer {

    @Bean
    public LoginInterceptor loginInterceptor() {
        return new LoginInterceptor();
    }


    /**
     * 注册拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(loginInterceptor())
                //拦截路径
                .addPathPatterns("/api/v1/pri/*/*/**")
                //放行路径
                .excludePathPatterns("/api/v1/pri/user/register", "/api/v1/pri/user/login");

        WebMvcConfigurer.super.addInterceptors(registry);
    }
}
