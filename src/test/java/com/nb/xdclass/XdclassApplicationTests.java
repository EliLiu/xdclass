package com.nb.xdclass;

import com.nb.xdclass.entity.model.User;
import com.nb.xdclass.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class XdclassApplicationTests {


    @Test
    void JwtUtilTester() {

        User user = new User();
        user.setId(001);
        user.setName("aaaa");
        user.setHeadImg("abc");
        String token = JwtUtil.generateJsonWebToken(user);
        System.out.println(token);
        Claims claims = JwtUtil.checkJWT(token);
        System.out.println((String)claims.get("name"));
    }

}
